# **WPensar - Desafio back end** #

## **Sobre** ##

Esse projeto é uma pequena aplicação web que simula um sistema de registro de estoque de um supermercado. Foi desenvolvido para o processo seletivo da WPensar, pela Carolina Ransatto.

## **Requisitos** ##

* [Python 3.5.2](https://www.python.org/downloads/)
* [Django 2.0.3](https://www.djangoproject.com/download/)

## **Como usar** ##

[Inicie o servidor](https://docs.djangoproject.com/en/2.0/ref/django-admin/##runserver) e abra http://localhost:8000/ para acessar a página inicial. Na aplicação é possível cadastrar e editar produtos e compras, e visualizar todos os que já estão no banco de dados.

Para acessar como um usuário comum, clique em entrar e use:

>Usuário: user
>
>Senha: password123

Para acessar como admin, abra http://localhost:8000/admin/ e use:

>Usuário: admin
>
>Senha: password123

## **Informações técnicas** ##

* [Trello do projeto](https://trello.com/b/WjnDXJL4/wpensar)

* A aplicação permite realizar o cadastro e a edição de produtos e compras. Apenas o admin pode deletar um produto ou uma compra, pelo painel do admin. Se um produto não tem nenhuma compra associada, seu custo médio é 0.

* Toda vez que uma compra é criada, editada ou deletada a média do produto é recalculada. Para saber que é necessário recalcular são utilizados os signals emitidos quando uma compra é salva ou deletada. (Arquivo stock/signals.py, que é importado no arquivo stock/apps.py)

* Administradores podem registrar novos usuários, que poderão fazer login e logout pelo aplicativo principal. As páginas de criação e edição de produtos e compras exigem login.

* A aplicação possui dois testes automatizados, que verificam se o custo médio de um produto é recalculado corretamente.