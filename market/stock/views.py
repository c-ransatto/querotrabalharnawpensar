from django.shortcuts import render
from django.views import generic
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import Product, Purchase


def home(request):
    return render(request, 'stock/home.html')


class ProductIndexView(generic.ListView):
    model = Product


class ProductCreateView(LoginRequiredMixin, generic.CreateView):
    login_url = '/login/'
    model = Product
    fields = ['name']
    success_url = reverse_lazy('stock:product_index')


class ProductUpdateView(LoginRequiredMixin, generic.UpdateView):
    login_url = '/login/'
    model = Product
    fields = ['name']
    success_url = reverse_lazy('stock:product_index')


class PurchaseIndexView(generic.ListView):
    model = Purchase


class PurchaseCreateView(LoginRequiredMixin, generic.CreateView):
    login_url = '/login/'
    model = Purchase
    fields = ['product', 'amount', 'cost_per_item']
    success_url = reverse_lazy('stock:purchase_index')


class PurchaseUpdateView(LoginRequiredMixin, generic.UpdateView):
    login_url = '/login/'
    model = Purchase
    fields = ['product', 'amount', 'cost_per_item']
    success_url = reverse_lazy('stock:purchase_index')
