from django.contrib import admin

from .models import Product, Purchase


class PurchaseInline(admin.TabularInline):
    model = Purchase
    extra = 1


class ProductAdmin(admin.ModelAdmin):
    readonly_fields = ('average_cost',)
    list_display = ('name', 'average_cost')
    list_filter = ['average_cost']
    search_fields = ['name']
    inlines = [PurchaseInline]


admin.site.register(Product, ProductAdmin)
admin.site.register(Purchase)
