from django.dispatch import receiver
from django.db.models.signals import post_save, post_delete

from .models import Purchase


@receiver(post_save, sender=Purchase)
def post_save_purchase_update_product_average_cost(sender, instance, **kwargs):
    """
    Recalcula o custo médio do produto após a criação ou edição de uma compra
    """
    instance.product.recalculate_average()


@receiver(post_delete, sender=Purchase)
def post_delete_purchase_update_product_average_cost(sender, instance,
                                                     **kwargs):
    """
    Recalcula o custo médio do produto após uma compra ser deletada
    """
    instance.product.recalculate_average()
