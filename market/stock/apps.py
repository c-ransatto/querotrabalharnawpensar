from django.apps import AppConfig


class StockConfig(AppConfig):
    name = 'stock'
    verbose_name = 'Estoque'

    def ready(self):
        import stock.signals
