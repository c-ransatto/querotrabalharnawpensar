from django.test import TestCase

from .models import Product, Purchase


class ProductModelTests(TestCase):

    def test_product_average_cost_recalculated_upon_purchase_creation(self):
        product = Product.objects.create(name='test')
        Purchase.objects.create(product=product, amount=10, cost_per_item=3)
        Purchase.objects.create(product=product, amount=15, cost_per_item=5)

        expected_value = (10 * 3 + 15 * 5) / (10 + 15)
        self.assertEqual(float(product.average_cost), expected_value)

    def test_product_average_cost_recalculated_upon_purchase_deletion(self):
        product = Product.objects.create(name='test')
        first = Purchase.objects.create(product=product, amount=10, cost_per_item=3)
        second = Purchase.objects.create(product=product, amount=15, cost_per_item=5)

        second.delete()
        expected_value = first.cost_per_item
        self.assertEqual(float(product.average_cost), expected_value)
