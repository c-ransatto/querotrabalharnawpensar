from django.db import models
from django.core.validators import MinValueValidator


class Product(models.Model):
    """
    Classe que representa os produtos que serão cadastrados.
    """
    name = models.CharField('nome', max_length=200, unique=True)
    average_cost = models.DecimalField('custo médio', default=0, max_digits=10,
                                       decimal_places=2, editable=False)

    def __str__(self):
        return self.name

    def recalculate_average(self):
        """
        Recalcula o custo médio do produto com base nas compras associadas.
        Método utilizado no arquivo signals.py.
        """
        if not self.purchase_set.all():
            # O produto não tem compras associadas
            self.average_cost = 0
            self.save()
            return

        # O produto tem compras associadas
        total_amount = 0
        total_cost = 0
        for p in self.purchase_set.values('cost_per_item', 'amount'):
            total_cost += p['cost_per_item'] * p['amount']
            total_amount += p['amount']
        self.average_cost = total_cost / total_amount
        self.save()

    class Meta:
        verbose_name = 'produto'


class Purchase(models.Model):
    """
    Classe que representa cada compra de um produto.
    """
    product = models.ForeignKey(Product, on_delete=models.CASCADE,
                                verbose_name='produto')
    amount = models.PositiveIntegerField('quantidade')
    cost_per_item = models.DecimalField('custo por item', max_digits=10,
                                        decimal_places=2,
                                        validators=[MinValueValidator(0)])

    class Meta:
        verbose_name = 'compra'
        ordering = ('product',)
