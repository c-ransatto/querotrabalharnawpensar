from django.urls import path
from . import views

app_name = 'stock'

urlpatterns = [
    path('', views.home, name='home'),

    path('products/', views.ProductIndexView.as_view(),
         name='product_index'),
    path('products/new', views.ProductCreateView.as_view(),
         name='product_create'),
    path('products/<int:pk>/edit', views.ProductUpdateView.as_view(),
         name='product_update'),

    path('purchases/', views.PurchaseIndexView.as_view(),
         name='purchase_index'),
    path('purchases/new', views.PurchaseCreateView.as_view(),
         name='purchase_create'),
    path('purchases/<int:pk>/edit', views.PurchaseUpdateView.as_view(),
         name='purchase_update'),
]
